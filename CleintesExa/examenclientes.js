const mysql = require('mysql');
const connection = mysql.createConnection({
host: "localhost",
user: "root",
password: "",
database: "clientesexa",
});
connection.connect((error)=>{
if(error) {
console.log('El error de conexion es : '+error);
return;
}
console.log('Conectado a la base de datos!');
});
//Tener en cuenta el nombre de la base de datos
//1 .- Invocamos a express
const express = require('express');
const aax = express();

//2 .- Seteamos urlencoded para capturar los datos del formulario
aax.use(express.urlencoded({extended:false}));
aax.use(express.json());

//3 .- Establecemos el motor de plantillas ejs
aax.set('view engine', 'ejs');

//4 .- Invocamos req y res de express
const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');

//5 .- Estableciendo las rutas
aax.get("/", (req, res) => {
    res.render("Formclientes");
  });

  aax.post("/", async (req, res) => {
    //validacion
    const formulario = document.getElementById('register_box');
    const inputs = document.querySelectorAll('#register_box input');

    //Expresiones regulares
      const expresiones = {
        cedula: /^\d{10}$/, // numérico de 10 dígitos.
        nombres: /^[a-zA-ZÀ-ÿ\s]{1,80}$/, //Texto de al menos 80 digitos.
        direccion: /^([a-zA-Z0-9_\s]){1,80}$/, //Alfanumerico de al menos 80 digitos.
        telefono: /^\d{10}$/, // numérico de 10 dígitos.
        correo: /\S+@\S+.\S+/, // formato valido de correo.
      };
    
    const validForm = (e) => {
    
        switch (e.target.name) {
    
            
            case "cedula_cli":
                if(expresiones.cedula.test(e.target.value)){
                    document.getElementById("smserr1").innerHTML = "";
                    a = false;
                } else{
                    document.getElementById("smserr1").innerHTML = "Ingrese 10 datos numericos";
                    a = true;
                }
            break;

            case "nombre":
                if(expresiones.nombres.test(e.target.value)){
                    document.getElementById("smserr1").innerHTML = "";
                    a = false;
                } else{
                    document.getElementById("smserr1").innerHTML = "No ingrese datos numericos / Campo en blanco";
                    a = true;
                }
            break;

            case "direccion":
                if(expresiones.direccion.test(e.target.value)){
                    document.getElementById("smserr1").innerHTML = "";
                    a = false;
                } else{
                    document.getElementById("smserr1").innerHTML = "Ingrese su direccion";
                    a = true;
                }
            break;

            case "correo_elec":
                if(expresiones.correo.test(e.target.value)){
                    document.getElementById("smserr3").innerHTML = "";
                    c = false;
                } else{
                    document.getElementById("smserr3").innerHTML = "Formato de correo no valido";
                    c = true;
                }
            break;
            case "telefono":
                if(expresiones.tele.test(e.target.value)){
                    document.getElementById("smserr4").innerHTML = "";
                    d = false;
                } else{
                    document.getElementById("smserr4").innerHTML = "Su telefono debe tener 10 caracteres numericos";
                    d = true;
                }
            break;
    
        }
    
    inputs.forEach((input) => {
        input.addEventListener('keyup', validForm);
        input.addEventListener('blur', validForm);
    });
    
    //insercion
    
    data = req.body;
      connection.query("INSERT INTO registrocliente SET ?", {
        cedula: data.cedula,
        nombre: data.nombre,
        direccion: data.direccion,
      telefono: data.semestre,
        correo: data.correo,
      });
      res.render("vistacliente");
    });

  aax.listen(3000, (req, res) => {
    console.log("SERVER RUNNING IN http://localhost:3000");
  });